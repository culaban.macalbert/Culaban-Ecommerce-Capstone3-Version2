import { Form , Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../../UserContext';
import { Navigate, NavLink, Route } from 'react-router-dom';

import TextField from '@mui/material/TextField';
import React from 'react';
import './Login.css'

import Swal from 'sweetalert2';
import Checkbox from '@mui/material/Checkbox';
import { Link } from "react-router-dom";

const labelConst = { inputProps: { 'aria-label': 'Checkbox demo' } };

export default function Login() {

    // State Hooks
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    const { user, setUser } = useContext(UserContext);

    function authentication(e){
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/Users/login`, {
            method: 'POST',
            headers: {"Content-type" : 'application/json'},
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(rest => rest.json())
        .then(data => {
            console.log(data);
            console.log("Check Access Token");
            console.log(data.accessToken);

            if(typeof data.accessToken !== "undefined"){
                localStorage.setItem('token', data.accessToken);
                localStorage.setItem('email', email);
                retrieveUserDetails(data.accessToken);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: `Welcome Home, ${email}!`
                  })
                }
                  else{
                    Swal.fire({
                      title: "Authentication failed",
                      icon: "error",
                      text: "Check your Login Details and Try Again"
                    })
                  }
                })
              setEmail('');
            }

            const retrieveUserDetails = (token) => {
                fetch(`${process.env.REACT_APP_API_URL}/Users/details`, {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);
              
                    setUser({
                        email: data.email,
                        id: data._id,
                        isAdmin: data.isAdmin
                    })
                })
              }

              useEffect(() => {
                if(email !== '' && password !== ''){
                    setIsActive(true)
                }
                else{
                    setIsActive(false)
                }
            }, [email, password])


            return (
                (user.id !== null)?
                (user.isAdmin ? <Navigate to="/admin" /> : <Navigate to="/" />) :
              <div className='loginContainer col-md-12'>
                <form className='loginFormClass col-md-12'  onSubmit={(event) => authentication(event)}>
                    <div className='loginClass'>
                    <h1>LOGIN</h1>
                    </div>
                <div className='emailClass col-md-12'>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <TextField id="outlined-email-input" label="Email" className="inputEmail" type="email" placeholder="Enter email" 
                    value = {email} onChange = {e => setEmail(e.target.value)}/>
                  </Form.Group>
                </div>
                
                <div className='passwordClass col-md-12'>
                  <Form.Group className=" mb-3" controlId="formBasicPassword">
                    <TextField id="outlined-password-input" label="Password" className='inputPassword' type="password" autoComplete="current-password" value = {password} onChange = {e => setPassword(e.target.value)}/>
                  </Form.Group>
                </div>

                <div className='checkBox'>
                <Checkbox {...labelConst} defaultChecked />Keep me logged in
                </div>
            
            
                
                  {isActive ? 
                  <Button type="submit" variant="primary" className='loginButton'>
                    Login
                  </Button>
                  :
                  <Button type="submit" variant="primary" className='disabledButton' disabled>
                    Login
                  </Button>
                    }

                <div className='registerClass'>
                <text>Don't have an account? </text><Link component="button" variant="body2" as={NavLink} to="/register">Sign up</Link>
                </div>
                </form>
              </div>
              );
            }
