import React, { useState, useEffect } from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function Users() {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/allUsers`)
      .then(response => response.json())
      .then(data => {
        setUsers(data);
      })
      .catch(error => console.error(error));
  }, []);

  return (
    <div>
      <h1>ALL USERS</h1>
      <table >
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Is Admin</th>
            <th>Promotion</th>
          </tr>
        </thead>
        <tbody>
          {users.map(user => (
            <tr key={user._id}>
              <td>{user._id}</td>
              <td>{user.firstName} {user.lastName}</td>
              <td>{user.email}</td>
              <td>{user.isAdmin ? 'Yes' : 'No'}</td>
              <td>{user.isAdmin ? <Button variant="danger" size="sm">Demote</Button>
              :
              <Button variant="primary" size="sm">Promote</Button>}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
