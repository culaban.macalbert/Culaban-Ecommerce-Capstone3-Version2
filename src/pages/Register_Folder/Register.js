import Swal from "sweetalert2";
import { useState, useEffect, useContext } from "react";
import { Link, Navigate, NavLink, useNavigate } from "react-router-dom";
import { Form, Button } from "react-bootstrap";
import './Register.css';

import UserContext from "../../UserContext";

export default function Register(){

    const {user} = useContext(UserContext);
    const navigate = useNavigate();

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [mobileNumber, setMobileNumber] = useState('');
    const [isActive, setIsActive] = useState(false);

    console.log(firstName);
    console.log(lastName);
    console.log(email);
    console.log(mobileNumber);
    console.log(password1);
    console.log(password2);

    useEffect(() => {
        if((firstName !== '' && lastName !== '' && email !== '' && mobileNumber !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [firstName, lastName, email, mobileNumber, password1, password2])


    function registerUser(e){
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: "POST",
            headers:{
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data =>{
            console.log(data);

            if(data){
                Swal.fire({
                    title: "Email Already Used",
                    icon: "error",
                    text: "Kindly provide another email to complete the registration."
                })
            }
            else{

                fetch(`${process.env.REACT_APP_API_URL}/Users/register`,{
                    method: "POST",
                    headers:{
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        password: password1,
                        mobileNo: mobileNumber
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);

                    if(data){
                        Swal.fire({
                            title: "Registration Successful",
                            icon: "success",
                            text: `Welcome! ${firstName}!`
                        });

                        // Clear input fields
                        setFirstName('');
                        setLastName('');
                        setEmail('');
                        setMobileNumber('');
                        setPassword1('');
                        setPassword2('');

                        navigate("/Login");
                    }
                    else{

                        Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please try again."
                        });

                    }
                })
            }
        })
    }

    return(
        (user.id !== null)?
        <Navigate to = "/Login"/>
        :
        <Form className="formContainer" onSubmit={(event) => registerUser(event)}>
          <div className="registerContainer">
            <h1>REGISTER</h1>
            <div className="firstNameClass">
            <Form.Group controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type="firstName" className="inputFirstName"
                    placeholder="First Name" 
                    value = {firstName}
                    onChange = {event => setFirstName(event.target.value)}
                    required
                    // class = "custom-input"
                />
            </Form.Group>
            </div>
            
            <div className="lastNameClass">
            <Form.Group controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control className="inputLastName"
                    type="lastName" 
                    placeholder="Last Name" 
                    value = {lastName}
                    onChange = {event => setLastName(event.target.value)}
                    required
                />
            </Form.Group>
            </div>

            <div className="userEmailClass">
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" className="inputEmail"
                    placeholder="Enter email" 
                    value = {email}
                    onChange = {event => setEmail(event.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>
            </div>

            <div className="mobileNumberClass">
            <Form.Group controlId="mobileNumber">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                    type="mobileNumber" className="inputMobileNumber"
                    placeholder="Mobile Number" 
                    value = {mobileNumber}
                    onChange = {event => setMobileNumber(event.target.value)}
                    required
                />
            </Form.Group>
            </div>

            <div className="password1Class">
            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" className="inputPassword1"
                    placeholder="Password" 
                    value = {password1}
                    onChange = {event => setPassword1(event.target.value)}
                    required
                />
            </Form.Group>
            </div>

            <div className="password2Class">
            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" className="inputPassword2"
                    placeholder="Verify Password" 
                    value = {password2}
                    onChange = {event => setPassword2(event.target.value)}
                    required
                />
            </Form.Group>
            </div>
          

            <div className='buttonSubmitClass pt-3'>
                { isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">
                        Submit
                    </Button>
                    :
                    <Button variant="primary" type="submit" id="submitBtn" disabled>
                        Register
                    </Button>
                }
            <div className='registerClass'>
                <text>Already have an Account? </text><Link component="button" variant="body2" as={NavLink} to="/login">Log in</Link>
            </div>
                </div>
            </div>
        </Form>
        )
}